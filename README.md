# nts_of
Apontamentos para Ótica Física na UBI, 2017, 2018, 2019, 2020 (em português)

Student notes for Physical Optics at UBI, 2017, 2018, 2019, 2020 (in Portuguese)

Bugs, para fazer, etc
1. [2019-06-16] A definição de irradiância é discutida com algum detalhe no
   capítulo 4, mas apresentada sumariamente no capítulo 3. É preciso arrajar
   isso de alguma maneira. Aproveitar para verificar a nomenclatura.
   [POR FAZER]
   [FEITO (junho 2020)]
2. [2019-06-16] Mudar a parte sobre interações com a matéria, espetros contínuos
   e descontínuos para o capítulo 4, sobre a radiação térmica e fotões.
   [FEITO (junho 2020)]
